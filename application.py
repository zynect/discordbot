from keys import config
from flask import request, Flask, render_template
import collections
import re
import mysql.connector
app = Flask(__name__)

@app.route("/")
def hello():
	return render_template('index.html', 
	message=['Get your server id by going to discord settings -> turn on developer mode -> right click server -> Copy ID'])

@app.route('/display_users', methods=['GET'])
def display_users():
	guild = request.args.get('server_id')
	if not guild:
		return render_template('index.html', message=["No server id given"])
	result = 'All users that have sent a message in this server:\n'
	try:
		cnx = mysql.connector.connect(**config)
		cur = cnx.cursor()
		select = 'SELECT DISTINCT author FROM messages WHERE guild = %(guild)s ORDER BY author DESC'
		cur.execute(select, { 'guild': guild })
		rows = cur.fetchall()
		if not rows:
			return render_template('index.html', message=["No messages from this server have been recorded"])
		for row in rows:
			result += ('%s\n' %(str(row[0])))
	except Exception as exp:
		print("Failure")
		print(exp)
	result = result.split('\n')
	return render_template('index.html', message=result)
	
@app.route('/display_top', methods=['GET'])
def display_top():
	guild = request.args.get('server_id')
	if not guild:
		return render_template('index.html', message=["No server id given"])
	result = 'Users with the most messages by USER ID | TOTAL MESSAGES\n'
	try:
		cnx = mysql.connector.connect(**config)
		cur = cnx.cursor()
		select = 'SELECT author, count(*) FROM messages WHERE guild = %(guild)s GROUP BY author ORDER BY count(*) DESC'
		cur.execute(select, { 'guild': guild })
		rows = cur.fetchall()
		if not rows:
			return render_template('index.html', message=["No messages from this server have been recorded"])
		for row in rows:
			result += ('%s | %s\n' %(str((row[0])), str(row[1])))
	except Exception as exp:
		print("Failure")
		print(exp)
	result = result.split('\n')
	return render_template('index.html', message=result)


@app.route('/display_words', methods=['GET'])
def display_words():
	guild = request.args.get('server_id')
	user = request.args.get('user_id')
	if not guild or not user:
		return render_template('index.html', message=["Either no server or user id given"])
	regex = re.compile('[,."()!?<>:;]')
	result = ['Top 20 words for user ' + user]
	lst = []
	try:
		cnx = mysql.connector.connect(**config)
		cur = cnx.cursor()
		select = 'SELECT message FROM messages WHERE guild = %(guild)s AND author = %(author)s'
		cur.execute(select, { 'guild': guild, 'author': user })
		rows = cur.fetchall()
		if not rows:
			return render_template('index.html', message=["No messages from this server have been recorded"])
		for row in rows:
			substr = regex.sub('', row[0])
			lst += substr.split();
	except Exception as exp:
		print("Failure")
		print(exp)
	counter = collections.Counter(lst)
	top = counter.most_common()
	num = 20 if len(counter) > 20 else len(counter)
	for i in range(0,num):
		result += [top[i][0] + ' | ' + str(top[i][1])]
	return render_template('index.html', message=result)
	

if __name__ == "__main__":
	app.run()
