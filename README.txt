This is a discord bot made for Cloud Computing.
Note: This requires a keys.py file to run, which contains login data to the database

This uses python3, so to install and run do: 
pip3  install -r requirements.txt 
python3 bot.py

To run it in the background do:
nohup python3 bot.py &

To close the bot running in the background do:
ps ax | grep test.py
find the PID of the bot, then do:
kill PID
