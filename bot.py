import discord
from discord.ext import commands
import mysql.connector
from mysql.connector import errorcode
from datetime import datetime
from keys import token, config
try:
     conn = mysql.connector.connect(**config)
     print("Connection established")
except mysql.connector.Error as err:
    if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
        print("Something is wrong with the user name or password")
    elif err.errno == errorcode.ER_BAD_DB_ERROR:
        print("Database does not exist")
    else:
        print(err)
else:
    cursor = conn.cursor()

bot = commands.Bot(command_prefix='~')
    
@bot.command()
async def test(ctx):
    await ctx.send('sanity check')
   
#Returns false if not connected
@bot.command()
async def ping(ctx):
    cnctd = conn.is_connected()
    await ctx.send(str(cnctd))

@bot.command()
async def prefix(ctx):
    await ctx.send('The current prefix is: ' + bot.command_prefix)

@bot.command()
async def setprefix(ctx, message: str):
    if len(message) > 1:
        await ctx.send('One character only for prefixes')
        return
    if message.isalnum():
        await ctx.send('No alphanumberic character for prefixes')
        return
    if message[0] == '<':
        await ctx.send('Don\'t use < as a prefix')
        return

    bot.command_prefix = message
    await ctx.send('My prefix is now: ' + bot.command_prefix)

#Show the users with the most recorded messages
@bot.command()
async def top(ctx, num=10):
    guild = ctx.guild.id
    select = 'SELECT author, count(*) FROM messages WHERE guild = %(guild)s GROUP BY author ORDER BY count(*) DESC LIMIT %(num)s'
    cursor.execute(select, { 'guild': guild, 'num': num })
    rows = cursor.fetchall()
    result = 'Users with the most messages by USER | TOTAL MESSAGES\n'
    for row in rows:
        result += ('%s | %s\n' %(str(bot.get_user(row[0])), str(row[1])))
    await ctx.send(result)

#Show the number of messages made by a specific user
@bot.command()
async def user(ctx, message: str):
    guild = ctx.guild.id
    for ch in ['<','@','!','>']:
        if ch in message:
            message = message.replace(ch,"")
    select = 'SELECT author, count(*) FROM messages WHERE guild = %(guild)s AND author = %(user)s'
    cursor.execute(select, { 'guild': guild, 'user': message })
    row = cursor.fetchone()
    if row[0] is not None:
        msg = ('User %s has made %s messages\n' %(str(bot.get_user(row[0])), str(row[1])))
        await ctx.send(msg)
    else:
        await ctx.send('Either the user doesn\'t exist here or the command syntax is wrong')

#Get the average time a user sends a message
@bot.command()
async def avgtime(ctx, message: str):
    guild = ctx.guild.id
    for ch in ['<','@','!','>']:
        if ch in message:
            message = message.replace(ch,"")
    select = 'SELECT author, timestamp FROM messages WHERE guild = %(guild)s AND author = %(user)s'
    cursor.execute(select, { 'guild': guild, 'user': message })
    rows = cursor.fetchall()
    total = 0
    num = 0
    if not rows:
        await ctx.send('The user does not exist')
        return
    for row in rows:
        total += row[1]
    avgtime = datetime.fromtimestamp(total/len(rows))
    await ctx.send('Average time active in chat: ' + datetime.strftime(avgtime,'%H:%M:%S') + ' UTC')

#Get the number of messages sent in each channel for a specific user
@bot.command()
async def channels(ctx, message: str):
    guild = ctx.guild.id
    for ch in ['<','@','!','>']:
        if ch in message:
            message = message.replace(ch,"")
    select = 'SELECT channel, count(*) FROM messages WHERE guild = %(guild)s AND author = %(user)s GROUP BY channel ORDER BY count(*) DESC'
    cursor.execute(select, { 'guild': guild, 'user': message })
    rows = cursor.fetchall()
    result = 'Channels with the most sent messages from this user:\n'
    if not rows:
        await ctx.send('The user does not exist')
        return
    for row in rows:
        result += ('%s | %s\n' %(str(bot.get_channel(row[0])), str(row[1])))
    await ctx.send(result)

@bot.event
async def on_ready():
    print('Connected!')
    print('Username: {0.name}\nID: {0.id}'.format(bot.user))
    
@bot.event
async def on_message(message):
    if message.author == bot.user:
        return
    
    timestamp = message.created_at.timestamp()
    #print(timestamp)
    #print(datetime.fromtimestamp(timestamp))
    #print(message.content+' '+str(message.author.id)+' '+str(message.channel.id)+' '+str(message.guild.id))
    if not conn.is_connected():
        print('Restting connection')
        conn.reconnect(attempts=5, delay=5)
    cursor.execute('INSERT INTO messages (message, author, channel, guild, timestamp) VALUES (%s, %s, %s, %s, %s);',  
        (message.content, message.author.id, message.channel.id, message.guild.id, timestamp))
    conn.commit()
    
    await bot.process_commands(message)

bot.run(token)
